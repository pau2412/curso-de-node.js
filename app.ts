import express from 'express'
import *as productos from './productos'

const app = express()
const port= 3000;
app.use(express.json());

//Creacion de los metodos
//Get
app.get('/productos/:id', function (req, res){
    
    res.send(productos.getStock());
})
app.post('/productos/:id', function (req, res) {
  const body = req.body;
  const id=Number(req.params.id);
  productos.storeProductos(body);
  res.send('Agregaremos un producto a la lista');

})
app.listen(3000,()=>
  console.log('Servidor',3000));


