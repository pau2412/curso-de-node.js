function suma(x:number,y:number):number{
    return x+y;
}
function resta(x:number,y:number):number{
    return x-y;
}
function multiplicar(x:number,y:number):number{
    return x*y;
}

// funcion flecha
const sumaFlecha = (x: number , y: number):number => {
    return x + y
}

const restaFlecha = (x: number , y: number):number => {
    return x - y
}

const multiplicarFlecha = (x: number , y: number):number => {
    return x * y
}
// funcion con parametros opcionales
const funcionOpcionalSuma = (x:number, y?:number):number =>{
    if(!y) return x 
    return x + y
}
const funcionOpcionalResta = (x:number, y?:number):number =>{
    if(!y) return x 
    return x - y
}
const funcionOpcionalMultiplicar = (x:number, y?:number):number =>{
    if(!y) return x 
    return x * y
}
// Otra opcion es agregarle un valor por defecto
const funcionOpcionalSuma2 = (x:number, y:number = 0):number =>{
    return x + y
}
const funcionOpcionalResta2 = (x:number, y:number = 0):number =>{
    return x - y
}
const funcionOpcionalMultiplicar2 = (x:number, y:number = 0):number =>{
    return x * y
}
console.log(funcionOpcionalSuma2(1));
console.log(funcionOpcionalSuma2(1,2));
console.log(funcionOpcionalResta2(5));
console.log(funcionOpcionalResta2(5,4));
console.log(funcionOpcionalMultiplicar2(7));
console.log(funcionOpcionalMultiplicar2(7,4));